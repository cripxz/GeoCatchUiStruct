package de.htwk.geocatch.geocatchuistruct.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentTaskBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.TaskPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.TaskView;


public class TaskFragment extends MvpFragment<TaskView, TaskPresenter> {

    @NonNull
    @Override
    public TaskPresenter createPresenter() {
        return new TaskPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentTaskBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task, container, false);
        binding.setPresenter(presenter);

        return binding.getRoot();
    }

}
