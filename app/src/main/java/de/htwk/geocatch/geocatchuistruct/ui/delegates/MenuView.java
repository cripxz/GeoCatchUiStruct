package de.htwk.geocatch.geocatchuistruct.ui.delegates;

import com.hannesdorfmann.mosby3.mvp.MvpView;


public interface MenuView extends MvpView {

    void createLobby();

    void joinLobby();

    void showLobbyList();

    void showOptions();

    void showTasks();
}
