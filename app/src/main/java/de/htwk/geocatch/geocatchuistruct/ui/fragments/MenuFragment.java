package de.htwk.geocatch.geocatchuistruct.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentMenuBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.MenuPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.activities.ScanQrActivity;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.MenuView;


public class MenuFragment extends MvpFragment<MenuView, MenuPresenter> implements MenuView {

    private MenuFragment.MenuClickListener mMenuClickCallback;

    @NonNull
    @Override
    public MenuPresenter createPresenter() {
        return new MenuPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentMenuBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        binding.setPresenter(presenter);

        return binding.getRoot();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mMenuClickCallback = (MenuFragment.MenuClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement MenuClickListener");
        }
    }

    @Override
    public void onDetach() {
        mMenuClickCallback = null;
        super.onDetach();
    }

    @Override
    public void createLobby() {
        if (mMenuClickCallback != null) {
            mMenuClickCallback.createLobby();
        }
    }

    @Override
    public void joinLobby() {

        //ergebnis ist die ID, das muss dann in der LobbyActivity (override onActivityResult..) abgefangen werden und von da aus der Lobby mit der erhaltenen ID begetreten werden..

        Intent intent = new Intent(getContext(), ScanQrActivity.class);
        getActivity().startActivityForResult(intent, ScanQrActivity.REQUEST_SCAN_QR);

    }

    @Override
    public void showLobbyList() {
        //TODO
    }

    @Override
    public void showOptions() {
        //TODO
    }

    @Override
    public void showTasks() {
        //TODO
    }

    public interface MenuClickListener {
        void createLobby();
    }
}
