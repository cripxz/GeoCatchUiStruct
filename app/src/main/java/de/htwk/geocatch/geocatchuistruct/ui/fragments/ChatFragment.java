package de.htwk.geocatch.geocatchuistruct.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentChatBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.ChatPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.ChatView;


public class ChatFragment extends MvpFragment<ChatView, ChatPresenter> {

    @NonNull
    @Override
    public ChatPresenter createPresenter() {
        return new ChatPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentChatBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        binding.setPresenter(presenter);

        return binding.getRoot();
    }
}
