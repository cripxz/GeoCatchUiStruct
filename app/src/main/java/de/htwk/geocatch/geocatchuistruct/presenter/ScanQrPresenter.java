package de.htwk.geocatch.geocatchuistruct.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import de.htwk.geocatch.geocatchuistruct.ui.delegates.ScanQrView;


public class ScanQrPresenter extends MvpBasePresenter<ScanQrView> {

    public void onDummyClick(){

        //Todo das ist nur ein dummy.. die QR scan Activity steht ja schon.. entsprechend wird der gescannte QR code beenden, wie hier in der ScanQrActivity gezeigt wird..

        ifViewAttached(ScanQrView::qrCodeScanned);
    }

}
