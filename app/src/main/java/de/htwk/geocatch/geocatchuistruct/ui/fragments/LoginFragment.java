package de.htwk.geocatch.geocatchuistruct.ui.fragments;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentLoginBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.LoginPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.LoginView;


public class LoginFragment extends MvpFragment<LoginView, LoginPresenter> implements LoginView {

    private OnLoggedInListener mLoggedInCallback;

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentLoginBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        binding.setPresenter(presenter);

        return binding.getRoot();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLoggedInCallback = (OnLoggedInListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnLoggedInListener");
        }
    }

    @Override
    public void onDetach() {
        mLoggedInCallback = null;
        super.onDetach();
    }

    @Override
    public void loggedIn() {
        if (mLoggedInCallback != null) {
            mLoggedInCallback.loggedIn();
        }
    }

    public interface OnLoggedInListener {
        void loggedIn();
    }

}
