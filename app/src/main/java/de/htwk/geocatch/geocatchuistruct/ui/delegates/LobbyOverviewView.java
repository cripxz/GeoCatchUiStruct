package de.htwk.geocatch.geocatchuistruct.ui.delegates;

import com.hannesdorfmann.mosby3.mvp.MvpView;


public interface LobbyOverviewView extends MvpView {
    void startGame();
}
