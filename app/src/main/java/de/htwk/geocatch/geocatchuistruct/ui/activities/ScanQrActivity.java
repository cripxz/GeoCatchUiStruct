package de.htwk.geocatch.geocatchuistruct.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.ActivityScanQrBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.ScanQrPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.ScanQrView;


public class ScanQrActivity extends MvpActivity<ScanQrView, ScanQrPresenter> implements ScanQrView {

    public static final int REQUEST_SCAN_QR = 1;
    public static final String EXTRA_LOBBY_ID = ScanQrActivity.class.getSimpleName() + ".extra.lobby_id";

    @NonNull
    @Override
    public ScanQrPresenter createPresenter() {
        return new ScanQrPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityScanQrBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_scan_qr);
        binding.setPresenter(presenter);
    }

    @Override
    public void qrCodeScanned() {

        //Todo gescannte ID zurückgeben!

        Intent intent = new Intent();
        intent.putExtra(EXTRA_LOBBY_ID, 2134); //gescannte ID
        setResult(Activity.RESULT_OK, intent);
        finish();

    }
}
