package de.htwk.geocatch.geocatchuistruct.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import de.htwk.geocatch.geocatchuistruct.ui.delegates.MenuView;


public class MenuPresenter extends MvpBasePresenter<MenuView> {

    //TODO hier muss noch logik rein -> id übergeben usw

    public void onCreateClick() {
        ifViewAttached(MenuView::createLobby);
    }

    public void onJoinClick() {
        ifViewAttached(MenuView::joinLobby);
    }

    public void onLobbyListClick() {
        ifViewAttached(MenuView::showLobbyList);
    }

    public void onOptionsClick() {
        ifViewAttached(MenuView::showOptions);
    }

    public void onTasksClick() {
        ifViewAttached(MenuView::showTasks);
    }

}
