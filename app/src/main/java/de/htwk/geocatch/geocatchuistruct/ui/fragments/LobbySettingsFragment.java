package de.htwk.geocatch.geocatchuistruct.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentLobbySettingsBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.LobbySettingsPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.LobbySettingsView;


public class LobbySettingsFragment extends MvpFragment<LobbySettingsView, LobbySettingsPresenter> {

    @NonNull
    @Override
    public LobbySettingsPresenter createPresenter() {
        return new LobbySettingsPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentLobbySettingsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lobby_settings, container, false);
        binding.setPresenter(presenter);

        return binding.getRoot();
    }
}
