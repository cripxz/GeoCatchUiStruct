package de.htwk.geocatch.geocatchuistruct.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.ActivityMainBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.MainPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.MainView;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.LoginFragment;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.MenuFragment;

public class MainActivity extends MvpActivity<MainView, MainPresenter> implements LoginFragment.OnLoggedInListener, MenuFragment.MenuClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private LoginFragment mLoginFragment = null;
    private MenuFragment mMenuFragment = null;

    //TODO in die Activity muss noch die Logik rein, dass man eingeloggt bleibt...

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setPresenter(presenter);

        if (mLoginFragment == null) {
            mLoginFragment = new LoginFragment();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.content_container, mLoginFragment).commit();
    }

    @Override
    public void loggedIn() {
        if (mMenuFragment == null) {
            mMenuFragment = new MenuFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_container, mMenuFragment).commit();
    }


    @Override
    public void createLobby() {
        startLobbyActivity(-1); //-1 als wert übergeben, um auf Lobbyseite unterscheiden zu können, ob man eine neue Lobby erstellt oder einer beitrit
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ScanQrActivity.REQUEST_SCAN_QR && resultCode == Activity.RESULT_OK) {
            int scannedLobbyId = data.getIntExtra(ScanQrActivity.EXTRA_LOBBY_ID, -1); //-1, wenn extra nicht enthalten -> entspricht fehler..
            Log.i(TAG, "received lobby id " + scannedLobbyId);

            if (scannedLobbyId != -1) {
                startLobbyActivity(scannedLobbyId);
            }

        }
    }

    private void startLobbyActivity(int id) {

        Intent intent = new Intent(this, LobbyActivity.class);
        intent.putExtra(LobbyActivity.EXTRA_LOBBY_ID, id);
        startActivity(intent);

    }
}
