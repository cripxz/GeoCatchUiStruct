package de.htwk.geocatch.geocatchuistruct.ui.fragments;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.FragmentLobbyOverviewBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.LobbyOverviewPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.LobbyOverviewView;

import static de.htwk.geocatch.geocatchuistruct.ui.activities.LobbyActivity.EXTRA_LOBBY_ID;

public class LobbyOverviewFragment extends MvpFragment<LobbyOverviewView, LobbyOverviewPresenter> implements LobbyOverviewView {

    private LobbyOverviewFragment.StartClickListener mLobbyOverviewCallback;

    @NonNull
    @Override
    public LobbyOverviewPresenter createPresenter() {
        return new LobbyOverviewPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int lobbyId = getArguments().getInt(EXTRA_LOBBY_ID); //TODO auslesen der Lobby id.. entsprechend wenn -1 -> neue erstellen sonst lobby daten laden

        FragmentLobbyOverviewBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lobby_overview, container, false);
        binding.setPresenter(presenter);
        presenter.mLobbyId.set("" + lobbyId);

        return binding.getRoot();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLobbyOverviewCallback = (LobbyOverviewFragment.StartClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement StartClickListener");
        }
    }

    @Override
    public void onDetach() {
        mLobbyOverviewCallback = null;
        super.onDetach();
    }

    @Override
    public void startGame() {
        if (mLobbyOverviewCallback != null) {
            mLobbyOverviewCallback.startGame();
        }
    }

    public interface StartClickListener {
        void startGame();
    }
}
