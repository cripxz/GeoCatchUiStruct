package de.htwk.geocatch.geocatchuistruct.presenter;

import android.databinding.ObservableField;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import de.htwk.geocatch.geocatchuistruct.ui.delegates.LobbyOverviewView;


public class LobbyOverviewPresenter extends MvpBasePresenter<LobbyOverviewView> {

    public ObservableField<String> mLobbyId = new ObservableField<>("");

    public void onStartClick() {
        ifViewAttached(LobbyOverviewView::startGame);
    }

}
