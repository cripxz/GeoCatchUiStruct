package de.htwk.geocatch.geocatchuistruct.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import de.htwk.geocatch.geocatchuistruct.ui.delegates.LoginView;


public class LoginPresenter extends MvpBasePresenter<LoginView> {

    public void login() {
        //TODO login kram... on success -> swap fragment..
        ifViewAttached(LoginView::loggedIn);
    }

}
