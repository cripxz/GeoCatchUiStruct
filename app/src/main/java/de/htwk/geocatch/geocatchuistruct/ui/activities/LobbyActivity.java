package de.htwk.geocatch.geocatchuistruct.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;

import de.htwk.geocatch.geocatchuistruct.R;
import de.htwk.geocatch.geocatchuistruct.databinding.ActivityLobbyBinding;
import de.htwk.geocatch.geocatchuistruct.presenter.LobbyPresenter;
import de.htwk.geocatch.geocatchuistruct.ui.delegates.LobbyView;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.ChatFragment;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.LobbyOverviewFragment;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.LobbySettingsFragment;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.MapFragment;
import de.htwk.geocatch.geocatchuistruct.ui.fragments.TaskFragment;


public class LobbyActivity extends MvpActivity<LobbyView, LobbyPresenter> implements LobbyOverviewFragment.StartClickListener {

    public static final String EXTRA_LOBBY_ID = LobbyActivity.class.getSimpleName() + ".extra.lobby_id";

    private ActivityLobbyBinding mBinding;
    private FragmentPagerItems mFragmentPagerItems;
    private FragmentStatePagerItemAdapter mAdapter;

    @NonNull
    @Override
    public LobbyPresenter createPresenter() {
        return new LobbyPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int lobbyId = getIntent().getIntExtra(EXTRA_LOBBY_ID, -1); //TODO auslesen der Lobby id.. entsprechend wenn -1 -> neue erstellen sonst lobby daten laden

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_lobby);
        mBinding.setPresenter(presenter);

        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_LOBBY_ID, lobbyId); //id an fragment weitergeben.. TODO übergabe der id an alle fragments evtl.

        mFragmentPagerItems = FragmentPagerItems.with(this)
                .add("Settings", LobbySettingsFragment.class)
                .add("Lobby", LobbyOverviewFragment.class, bundle)
                .add("Chat", ChatFragment.class).create();

        mAdapter = new FragmentStatePagerItemAdapter(getSupportFragmentManager(), mFragmentPagerItems) {

            @Override
            public int getItemPosition(Object object) {
                return PagerAdapter.POSITION_NONE;
            }
        };

        mBinding.viewPager.setAdapter(mAdapter);
        mBinding.viewPager.setCurrentItem(1);    //mittleren Tab als ausgewählten bei Start
        mBinding.tabLayout.setViewPager(mBinding.viewPager);
    }

    @Override
    public void startGame() {
        mFragmentPagerItems.set(0, FragmentPagerItem.of("Task", TaskFragment.class));
        mFragmentPagerItems.set(1, FragmentPagerItem.of("Map", MapFragment.class));
        mAdapter.notifyDataSetChanged();
        mBinding.viewPager.setCurrentItem(1);    //mittleren Tab als ausgewählten bei Start
        mBinding.tabLayout.setViewPager(mBinding.viewPager);
    }
}
